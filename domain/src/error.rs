use std::{error::Error, fmt::Display};

#[derive(Debug, Clone)]
pub struct DomainError {
    msg: String,
}
impl Display for DomainError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}
impl Error for DomainError {}

impl From<String> for DomainError {
    fn from(value: String) -> Self {
        Self { msg: value }
    }
}
