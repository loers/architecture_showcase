use std::fmt::Display;

use serde::{Deserialize, Serialize};

use crate::DomainError;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct UserName {
    value: String,
}
impl UserName {
    pub fn value_owned(self) -> String {
        self.value
    }
}
impl From<UserName> for String {
    fn from(value: UserName) -> Self {
        value.value
    }
}
impl Display for UserName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}
impl TryFrom<String> for UserName {
    type Error = DomainError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.len() < 2 {
            Err(format!(
                "User names must have at least 2 characters but received '{}'.",
                value
            )
            .into())
        } else {
            Ok(Self { value })
        }
    }
}

impl TryFrom<&str> for UserName {
    type Error = DomainError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::try_from(value.to_string())
    }
}
