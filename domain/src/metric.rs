use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Metric {
    name: String,
    values: Vec<u128>,
}

impl Metric {
    pub fn new(name: &str, values: Vec<u128>) -> Self {
        Self {
            name: name.into(),
            values,
        }
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn values(&self) -> &Vec<u128> {
        &self.values
    }
}
