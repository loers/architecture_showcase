use std::fmt::Display;

use serde::{Deserialize, Serialize};

use crate::DomainError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct ReportNumber {
    num: i32,
}

impl ReportNumber {
    pub fn value(&self) -> i32 {
        self.num
    }
}

impl From<ReportNumber> for i32 {
    fn from(value: ReportNumber) -> Self {
        value.num
    }
}
impl TryFrom<i32> for ReportNumber {
    type Error = DomainError;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        if value < 1000 {
            Err(format!("Report numbers must start at 1000 but received {}.", value).into())
        } else {
            Ok(Self { num: value })
        }
    }
}
impl Display for ReportNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.num)
    }
}
