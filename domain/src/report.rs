use serde::{Serialize, Deserialize};

use crate::{Metric, ReportNumber, UserName};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Report {
    num: ReportNumber,
    creator: UserName,
    metrics: Vec<Metric>,
}

impl Report {
    pub fn new(creator: impl Into<UserName>, number: impl Into<ReportNumber>) -> Self {
        Self {
            num: number.into(),
            creator: creator.into(),
            metrics: Default::default(),
        }
    }

    pub fn num(&self) -> ReportNumber {
        self.num
    }
    pub fn creator(&self) -> &UserName {
        &self.creator
    }
    pub fn creator_owned(self) -> UserName {
        self.creator
    }
    pub fn metrics(&self) -> &Vec<Metric> {
        &self.metrics
    }
}
