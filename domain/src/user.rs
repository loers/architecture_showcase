use std::{rc::Rc, fmt::Display};

use serde::{Deserialize, Serialize};

use crate::{Id, UserName};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct User {
    id: Id,
    name: UserName,
}

impl User {
    pub fn new(name: impl Into<UserName>) -> Self {
        User {
            id: Default::default(),
            name: name.into(),
        }
    }
    pub fn name(&self) -> &UserName {
        &self.name
    }
    pub fn name_owned(self) -> UserName {
        self.name
    }
    pub fn id(&self) -> &Id {
        &self.id
    }
}

impl Display for User {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl From<(Id, UserName)> for User {
    fn from((id, name): (Id, UserName)) -> Self {
        Self { id, name }
    }
}

// impl From<User> for wasm_bindgen::JsValue {
//     fn from(value: User) -> Self {
//         wasm_bindgen::JsValue::serde
//     }
// }

impl User {
    pub fn as_any(self) -> Rc<dyn std::any::Any> {
        Rc::new(self)
    }
}

#[cfg(test)]
mod tests {
    use crate::{User, UserName};

    #[test]
    fn just_an_example_test() {
        let user = UserName::try_from("foo").ok().map(User::new);
        assert!(user.is_some());
        assert_eq!(user.unwrap().name().to_string(), "foobar");
    }
}
