use playwright::{api::{Page, BrowserContext}, Playwright};

async fn setup() -> anyhow::Result<(Page, Playwright, BrowserContext)> {
    let playwright = Playwright::initialize().await?;
    playwright.prepare()?; // Install browsers
    let chromium = playwright.chromium();
    let browser = chromium.launcher().headless(true).launch().await?;
    let context = browser.context_builder().build().await?;
    let page = context.new_page().await?;
    return Ok((page, playwright, context));
}

#[tokio::test]
async fn test1() -> anyhow::Result<()> {
    let (page, _pw, _bc) = setup().await?;
    page.goto_builder("https://example.com/").goto().await?;

    // Exec in browser and Deserialize with serde
    let s: String = page.eval("() => location.href").await?;
    assert_eq!(s, "https://example.com/");
    page.click_builder("a").click().await?;
    Ok(())
}

#[tokio::test]
async fn test2() -> anyhow::Result<()> {
    let (page, _pw, _bc) = setup().await?;

    page.goto_builder("https://example.com/").goto().await?;

    // Exec in browser and Deserialize with serde
    let s: String = page.eval("() => location.href").await?;
    assert_eq!(s, "https://example.com/");
    page.click_builder("a").click().await?;
    Ok(())
}
