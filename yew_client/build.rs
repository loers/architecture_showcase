use std::path::PathBuf;

pub fn main() -> std::io::Result<()> {
    gettr::update_translations(
        vec!["de"],
        PathBuf::from("src"),
        PathBuf::from("src/i18n")
    )?;
    Ok(())
}
