use gettr::use_gettr;
use yew::prelude::*;

#[function_component(Users)]
pub fn users() -> Html {
    use_gettr();
    html! {
        <main class="flex flex-col w-full h-full p-4 gap-2">
            {"Users"}
        </main>
    }
}
