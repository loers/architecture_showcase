use crate::components::*;
use gettr::{gettr, use_gettr};
use yew::prelude::*;

#[function_component(Home)]
pub fn home() -> Html {
    use_gettr();
    html! {
        <main class="flex flex-col w-full h-full p-4 gap-2">
            <h1>{ gettr!("Hello World!")}</h1>
            <div class="flex  justify-start items-start">
                <Card title="User Area">
                   <div class="flex flex-col gap-2">
                        <label>{gettr!("Username")}</label>
                        <span>{"foo bar"}</span>
                        <label>{gettr!("Email")}</label>
                        <span>{"foo@bar.com"}</span>
                   </div>
                </Card>
            </div>
        </main>
    }
}
