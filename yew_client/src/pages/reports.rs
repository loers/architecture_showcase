use std::{ops::Deref, process::Output, rc::Rc};

use domain::User;
use gettr::gettr;
use web_sys::HtmlInputElement;
use yew::{html::ImplicitClone, prelude::*};

use crate::{
    classnames_macro::classnames,
    components::{Button, Icon, IconName, Listbox, ListboxItem},
    hooks::{use_reports, use_users},
};

// #[derive(Debug, Clone, PartialEq)]
// struct User {
//     name: String,
// }
// impl User {
//     pub fn name(&self) -> &String {
//         &self.name
//     }
// }
// impl ImplicitClone for User {}

#[function_component(Reports)]
pub fn reports() -> Html {
    let reports = use_reports();
    let users = use_users();
    // let users = use_state(|| vec![User { name: "foo".into() }]);

    // let selected_user: UseStateHandle<Option<Rc<User>>> = use_state(|| None);

    // // let onchange = {
    // //     let selected_user = selected_user.clone();
    // //     Callback::from(move |user: Option<Rc<User>>| {
    // //         log::info!("{:?}", user);
    // //         // selected_user.set(user.clone());
    // //     })
    // // };

    // let onchange = use_callback(selected_user, |item: Option<Rc<User>>, su| {
    //     su.set(item.clone());
    // });

    html! {
        <main class="flex flex-col w-full h-full items-start p-4 gap-2">
            <h1>{gettr!("Reports")}</h1>
            <div class="flex flex-col justify-start items-start">
                {reports.iter().map(|r| {
                    html!{
                        <div class="flex gap-2">
                            <div>{r.num().to_string()}</div>
                            <div>{r.creator().to_string()}</div>
                        </div>
                    }
                }).collect::<Html>()}
            </div>
            <h2>{gettr!("Create new report")}</h2>
            <div class="flex flex-col">

                // <Listbox<User>
                //     class="border rounded p-2"
                //     placeholder={gettr!("Select user")}
                //     onchange={onchange}
                // >
                //     <div class="relative cursor-pointer">
                //         <input
                //             class="border rounded-md p-2"
                //             value={selected_user.as_ref().map(|u| u.name().to_string())}
                //         />
                //         <div class="absolute right-0 top-0 mr-2 flex items-center justify-center h-full">
                //             <Icon name={IconName::Down} />
                //         </div>
                //     </div>

                //     <div class={classnames!(
                //         "bg-white rounded-md border shadow-md",
                //         "flex flex-col gap-2",
                //         "[&>*:first-child]:rounded-t-md",
                //         "[&>*:last-child]:rounded-b-md"
                //     )}>
                //         {for users.iter().map(|u| html!{
                //             <ListboxItem item={u.clone().as_any()} class={classnames!(
                //                 "flex gap-2 p-2 items-center whitespace-nowrap hover:bg-blue-200"
                //             )}>
                //                 {
                //                     if let Some(user) = selected_user.as_ref() {
                //                         html!{
                //                             <Icon class={classnames!(
                //                                 "data-[listbox-selected=true]:visible"
                //                             )} name={IconName::Check} />
                //                         }
                //                     } else {
                //                         html! {
                //                             {"asdf"}
                //                         }
                //                     }
                //                 }
                //                 {u.name().to_string()}
                //             </ListboxItem>
                //         })}
                //     </div>
                // </Listbox<User>>
            </div>
        </main>
    }
}
