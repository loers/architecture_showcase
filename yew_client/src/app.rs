use domain::*;
use gloo_net::http::Request;
use yew::prelude::*;
use crate::components::*;

#[function_component(App)]
pub fn app() -> Html {
    // let reports = use_state(|| vec![]);
    // {
    //     let reports = reports.clone();
    //     use_effect_with_deps(
    //         move |_| {
    //             let reports = reports.clone();
    //             wasm_bindgen_futures::spawn_local(async move {
    //                 let fetched_reports: Vec<Report> = Request::get("localhost:8090/reports")
    //                     .send()
    //                     .await
    //                     .unwrap()
    //                     .json()
    //                     .await
    //                     .unwrap();
    //                 reports.set(fetched_reports);
    //             });
    //             || ()
    //         },
    //         (),
    //     );
    // }
    html! {
        <main class="flex flex-col w-full h-full p-4 gap-2">
            <h1>{ "Hello World!" }</h1>
            <div class="flex  justify-start items-start">
                <Card title="User Area">
                   <div class="flex flex-col gap-2">
                        <label>{"Username"}</label>
                        <span>{"foo bar"}</span>
                        <label>{"Email"}</label>
                        <span>{"foo@bar.com"}</span>
                   </div>
                </Card>
            </div>
        </main>
    }
}