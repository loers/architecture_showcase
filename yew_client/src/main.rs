mod classnames_macro;
mod components;
mod hooks;
mod pages;
mod routing;

use classnames_macro::classnames;

use routing::Routing;
use yew::{function_component, html, Html};

use gettr::GettrProvider;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    gettr::init("en", vec![
        ("de", include_str!("i18n/de"))
    ]);
    yew::Renderer::<App>::new().render();
}

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <GettrProvider>
            <Routing></Routing>
        </GettrProvider>
    }
}
