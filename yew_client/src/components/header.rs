use crate::{classnames_macro::classnames, components::*};
use gettr::{gettr, use_gettr};
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct Props {}

#[function_component(Header)]
pub fn header(_props: &Props) -> Html {
    let i18n = use_gettr();
    let select_lang_de = {
        let ctx = i18n.clone();
        Callback::from(move |_| ctx.set("de".into()))
    };
    let select_lang_en = {
        let ctx = i18n.clone();
        Callback::from(move |_| ctx.set("en".into()))
    };

    let current = if i18n.get() == "de" {
        html! { <img src="/assets/de.svg" class="w-8" /> }
    } else {
        html! { <img src="/assets/uk.svg" class="w-8" /> }
    };

    return html! {
        <header class="shadow-md border-b p-2 pl-12 bg-emerald-800 text-white flex items-center justify-between">
            <h1>{gettr!("Application Title")}</h1>
            <Dropover class="relative">
                <button>
                    {current}
                </button>
                <div class={classnames!(
                    "data-[panel-open=true]:visible data-[panel-open=false]:invisible",
                    "p-2 w-12 right-0 absolute bg-white rounded border shadow-md",
                    "flex flex-col gap-2"
                )}>
                    <button onclick={select_lang_de}>
                        <img src="/assets/de.svg" class="w-8" />
                    </button>
                    <button onclick={select_lang_en}>
                        <img src="/assets/uk.svg" class="w-8" />
                    </button>
                </div>
            </Dropover>
        </header>
    };
}
