use yew::prelude::*;

use super::{Icon, IconName};
use crate::classnames;
#[derive(Properties, PartialEq)]
pub struct ButtonProps {
    #[prop_or_default]
    pub icon: IconName,
    #[prop_or_default(false)]
    pub secondary: bool,
    #[prop_or_default(false)]
    pub thin: bool,
    #[prop_or_default]
    pub children: Children,
    #[prop_or_default]
    pub onclick: Callback<MouseEvent>
}

#[function_component(Button)]
pub fn button(props: &ButtonProps) -> Html {
    let click_handler = props.onclick.clone();

    return html! {
        <button class={classnames!{
            "rounded",
            if props.thin { "" } else {  "p-2" },
            if props.secondary { "" } else { "bg-emerald-900 text-white" },
        }}
        onclick={move |e: MouseEvent| click_handler.emit(e)}
        >
            <Icon name={props.icon}></Icon>
            {for props.children.iter()}
        </button>
    };
}
