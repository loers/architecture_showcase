use yew::prelude::*;
use yew_icons::{Icon as Ycon, IconId};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum IconName {
    None,
    Left,
    Right,
    Up,
    Down,
    Gear,
    Check,
}

impl Default for IconName {
    fn default() -> Self {
        Self::None
    }
}

impl IconName {
    fn into_id(&self) -> Option<IconId> {
        match self {
            IconName::None => None,
            IconName::Left => Some(IconId::LucideChevronLeft),
            IconName::Right => Some(IconId::LucideChevronRight),
            IconName::Up => Some(IconId::LucideChevronUp),
            IconName::Down => Some(IconId::LucideChevronDown),
            IconName::Gear => Some(IconId::LucideCog),
            IconName::Check => Some(IconId::LucideCheck),
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub name: IconName,
    #[prop_or_default]
    pub class: String,
    // pub children: Children,
}

#[function_component(Icon)]
pub fn icon(props: &Props) -> Html {
    return html! {
        <i class={props.class.clone()}>
            {
                if let Some(icon) = props.name.into_id() {
                    html!{
                        <Ycon icon_id={icon}></Ycon>
                    }
                } else {
                    html!{}
                }
            }
        </i>
    };
}
