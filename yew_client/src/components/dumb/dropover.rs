use wasm_bindgen::{prelude::Closure, JsCast};
use web_sys::HtmlElement;
use yew::prelude::*;
use yew_hooks::use_click_away;

#[derive(Properties, PartialEq)]
pub struct DropoverProps {
    pub children: Children,
    #[prop_or_default]
    pub class: String,
}

#[function_component(Dropover)]
pub fn dropover(props: &DropoverProps) -> Html {
    let open_state = use_state(|| false);
    let root_ref = use_node_ref();

    {
        let o = open_state.clone();
        use_click_away(root_ref.clone(), move |_| {
            o.set(false);
        });
    }

    let toggle_open: Callback<(), ()> = use_callback(open_state.clone(), |_, open_state| {
        open_state.set(!**open_state);
    });

    // handle panel node
    use_effect_with(
        (root_ref.clone(), toggle_open.clone(), open_state.clone()),
        move |(root, toggle_open, open)| {
            let root_element: Option<HtmlElement> = root.cast();

            let toggle = toggle_open.clone();
            let listener = Closure::<dyn Fn(Event)>::wrap(Box::new(move |_| toggle.emit(())));

            let panel_node = root_element
                .clone()
                .and_then(|e| e.first_child())
                .and_then(|c| c.next_sibling())
                .and_then(|e| e.dyn_into::<HtmlElement>().ok());
            if let Some(element) = panel_node.as_ref() {
                element
                    .set_attribute("data-panel-open", &open.to_string())
                    .ok();
            }

            if let Some(node) = panel_node.clone() {
                node.add_event_listener_with_callback("click", listener.as_ref().unchecked_ref())
                    .ok();
            }
            move || {
                if let Some(node) = panel_node {
                    node.remove_event_listener_with_callback(
                        "click",
                        listener.as_ref().unchecked_ref(),
                    )
                    .ok();
                }
                drop(listener)
            }
        },
    );

    // handle button node
    use_effect_with(
        (root_ref.clone(), toggle_open.clone()),
        move |(root, toggle_open)| {
            let root_element: Option<HtmlElement> = root.cast();

            let toggle = toggle_open.clone();
            let listener = Closure::<dyn Fn(Event)>::wrap(Box::new(move |_| toggle.emit(())));

            let btn_node = root_element.and_then(|e| e.first_child());

            if let Some(node) = btn_node.clone() {
                node.add_event_listener_with_callback("click", listener.as_ref().unchecked_ref())
                    .ok();
            }
            move || {
                if let Some(node) = btn_node {
                    node.remove_event_listener_with_callback(
                        "click",
                        listener.as_ref().unchecked_ref(),
                    )
                    .ok();
                }
                drop(listener)
            }
        },
    );

    html! {
        <div ref={root_ref} class={&props.class}>
            {for props.children.iter()}
        </div>
    }
}

#[derive(Properties, PartialEq)]
pub struct PanelProps {
    open: bool,
}
