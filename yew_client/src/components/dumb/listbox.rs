use std::rc::Rc;

use wasm_bindgen::JsCast;
use web_sys::{HtmlElement, HtmlInputElement};
use yew::prelude::*;

use crate::{classnames, components::Dropover};

#[derive(Properties, PartialEq)]
pub struct ListboxProps<T: std::fmt::Display + PartialEq + 'static> {
    #[prop_or_default]
    pub placeholder: AttrValue,
    #[prop_or_default]
    pub class: AttrValue,

    #[prop_or_default]
    pub onchange: Callback<Option<Rc<T>>>,

    #[prop_or_default]
    pub children: Children,
}

#[function_component(Listbox)]
pub fn listbox<T: std::fmt::Display + PartialEq + 'static>(props: &ListboxProps<T>) -> Html {
    let value_state = use_state(|| SelectedItem::default());
    let panel_ref = use_node_ref();
    let btn_ref = use_node_ref();

    use_effect_with(
        (props.onchange.clone(), btn_ref.clone(), value_state.clone()),
        |(onchange, btn_ref, value_state)| {
            if let Some(btn) = btn_ref.get() {
                let h = btn.dyn_into::<HtmlElement>().unwrap();
                if let Ok(Some(input)) = h
                    .query_selector("input")
                    .map(|e| e.and_then(|e| e.dyn_into::<HtmlInputElement>().ok()))
                {
                    if let Some(v) = value_state.0.clone().and_then(|a| a.downcast::<T>().ok()) {
                        input.set_value(&v.to_string());
                        onchange.emit(Some(v));
                    } else {
                        input.set_value("");
                        onchange.emit(None);
                    }
                }
            }
        },
    );

    let c = use_callback(value_state.clone(), |item: SelectedItem, value_state| {
        value_state.set(item);
    });

    let mut children = props.children.iter();
    let btn = children.next();
    let panel = children.next();

    return html! {
        <ContextProvider<ListboxCtx> context={ListboxCtx {
            select: c
        }}>
            <Dropover class="relative">
                <div ref={btn_ref}>
                    {btn}
                </div>
                <div ref={panel_ref} data-panel="true" class={classnames!(
                    "min-w-full",
                    "absolute left-0 mt-2",
                    "data-[panel-open=true]:visible data-[panel-open=false]:invisible"
                )}>
                    {panel}
                </div>
            </Dropover>
        </ContextProvider<ListboxCtx>>
    };
}

#[derive(Debug, Clone, PartialEq, Default)]
struct ListboxCtx {
    select: Callback<SelectedItem>,
}

#[derive(Debug, Clone, Default)]
struct SelectedItem(Option<Rc<dyn std::any::Any>>);

impl PartialEq for SelectedItem {
    fn eq(&self, other: &Self) -> bool {
        let eq_ptr = if let Some(a) = &self.0 {
            if let Some(b) = &other.0 {
                Rc::ptr_eq(a, b)
            } else {
                false
            }
        } else {
            other.0.is_none()
        };

        return eq_ptr;
    }
}

#[derive(Properties)]
pub struct ItemProps {
    #[prop_or_default]
    pub item: Option<Rc<dyn std::any::Any>>,
    #[prop_or_default]
    pub children: Children,
    #[prop_or_default]
    pub class: AttrValue,
}

impl PartialEq for ItemProps {
    fn eq(&self, other: &Self) -> bool {
        let item_eq = if let Some(item) = &self.item {
            if let Some(other) = &other.item {
                Rc::ptr_eq(item, other)
            } else {
                false
            }
        } else {
            other.item.is_none()
        };
        item_eq && self.children == other.children && self.class == other.class
    }
}

#[function_component(ListboxItem)]
pub fn item(props: &ItemProps) -> Html {
    let option_ref = use_node_ref();
    let ctx = use_context::<ListboxCtx>().expect("Listbox item is not child of Listbox");

    let onclick = {
        let i = props.item.clone();
        Callback::from(move |_| ctx.select.emit(SelectedItem(i.clone())))
    };

    return html! {
        <option ref={option_ref} class={&props.class} onclick={onclick}>
            {for props.children.iter()}
        </option>
    };
}
