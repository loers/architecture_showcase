use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct CardProps {
    pub title: String,
    pub children: Children,
}

#[function_component(Card)]
pub fn card(props: &CardProps) -> Html {
    html! {
        <section class="flex flex-col flex-shrink gap-2 min-h-[6rem] p-4 border shadow-md rounded-md bg-white">
            <h2>{&props.title}</h2>
            <div>
                { props.children.clone() }
            </div>
        </section>
    }
}
