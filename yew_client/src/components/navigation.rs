use crate::{classnames_macro::classnames, components::*, routing::{Route, ReportsRoute}};
use yew::prelude::*;
use yew_hooks::prelude::*;
use yew_router::prelude::use_navigator;

#[derive(Properties, PartialEq)]
pub struct NavigationProps {}

#[function_component(Navigation)]
pub fn navigation(_props: &NavigationProps) -> Html {
    let storage_nav_open = use_local_storage::<bool>("nav_open".to_string());
    let open_state = use_state(|| false);
    let navigator = use_navigator().expect("Failed to get navigator");

    let set_open_state = open_state.clone();
    let get_storage_nav_open = storage_nav_open.clone();
    use_effect_once(move || {
        let open = get_storage_nav_open.unwrap_or_default();
        set_open_state.set(open);
        || {
            // no cleanup necessary
        }
    });
    use_effect_with(
        (open_state.clone(), storage_nav_open.clone()),
        |(open_state, storage_nav_open)| {
            storage_nav_open.set(**open_state);
        },
    );

    let open = {
        let open = open_state.clone();
        Callback::from(move |_| open.set(true))
    };
    let close = {
        let open = open_state.clone();
        Callback::from(move |_| open.set(false))
    };

    let go_home = {
        let nav = navigator.clone();
        Callback::from(move |_| nav.push(&Route::Home))
    };
    let go_users = {
        let nav = navigator.clone();
        Callback::from(move |_| nav.push(&Route::Users))
    };
    let go_reports = {
        let nav = navigator.clone();
        Callback::from(move |_| nav.push(&ReportsRoute::Overview))
    };

    html! {
        <aside class={classnames!(
            if *open_state { "shadow-lg border-r" } else { "absolute top-1 text-white" }
        )}>
            <div class="flex items-center justify-center">
                if !*open_state {<Button icon={IconName::Down} secondary={true} onclick={open} ></Button>}
                if *open_state {<Button icon={IconName::Left} secondary={true} onclick={close}></Button>}
            </div>
            if *open_state {
                <div class="p-2 flex flex-col items-start gap-2 border-t">
                    <button onclick={go_home}>{"home"}</button>
                    <button onclick={go_reports}>{"reports"}</button>
                    <button onclick={go_users}>{"users"}</button>
                </div>
            }
        </aside>
    }
}
