macro_rules! classnames {
    () => { String::new() };
    ($($x:expr),+ $(,)?) => {{
        let mut s = Vec::new();
        $(
            s.push($x);
        )+
        s.join(" ")
    }};
}

pub(crate) use classnames;

#[cfg(test)]
mod tests {
    #[test]
    fn test_macro() {
        assert_eq!(
            classnames!("foobar", if true { "test" } else { "nope" }),
            "foobar test".to_string()
        );
    }
}
