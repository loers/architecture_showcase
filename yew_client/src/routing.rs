use yew::prelude::*;
pub(crate) use yew_router::prelude::*;

use crate::{components::*, pages::*};

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[not_found]
    #[at("/404")]
    NotFound,
    #[at("/reports")]
    ReportsRoot,
    #[at("/reports/*")]
    Reports,
    #[at("/users")]
    Users,
}

#[derive(Clone, Routable, PartialEq)]
pub enum ReportsRoute {
    #[at("/reports/:id")]
    Editor,
    #[at("/reports/overview")]
    Overview,
    #[at("/reports/404")]
    NotFound,
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <Home /> },
        Route::Reports | Route::ReportsRoot => html! {
            <Switch<ReportsRoute> render={switch_reports} />
        },
        Route::Users => html! { <Users /> },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}

fn switch_reports(route: ReportsRoute) -> Html {
    match route {
        ReportsRoute::Overview => html! {<Reports />},
        ReportsRoute::Editor => html! {<h1>{"Editor"}</h1>},
        ReportsRoute::NotFound => html! {<Redirect<Route> to={Route::NotFound}/>},
    }
}

#[function_component(Routing)]
pub fn routing() -> Html {
    html! {
        <BrowserRouter>
            <div class="flex flex-col h-full w-full">
                <Header></Header>
                <div class="flex grow">
                    <Navigation></Navigation>
                    <Switch<Route> render={switch} />
                </div>
                <footer class="border-t">
                    {"footer"}
                </footer>
            </div>
        </BrowserRouter>
    }
}
