use domain::Report;
use gloo_net::http::Request;
use yew::prelude::*;

#[hook]
pub fn use_reports() -> UseStateHandle<Vec<Report>> {
    let reports = use_state(|| vec![]);
    {
        let reports = reports.clone();
        use_effect_with((), move |_| {
            let reports = reports.clone();
            wasm_bindgen_futures::spawn_local(async move {
                let fetched_reports: Vec<Report> = Request::get("/service/reports")
                    .send()
                    .await
                    .unwrap()
                    .json()
                    .await
                    .unwrap();
                reports.set(fetched_reports);
            });
            || ()
        });
    }
    reports
}

#[hook]
pub fn use_save_report() -> Callback<Report, ()> {
    use_callback((), |report: Report, _| {
        wasm_bindgen_futures::spawn_local(async move {
            Request::post("/service/reports")
                .json(&report)
                .unwrap()
                .send()
                .await
                .unwrap();
        });
    })
}
