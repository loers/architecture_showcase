use domain::User;
use gloo_net::http::Request;
use yew::prelude::*;

#[hook]
pub fn use_users() -> UseStateHandle<Vec<User>> {
    let users = use_state(|| vec![]);
    {
        let users = users.clone();
        use_effect_with((), move |_| {
            let users = users.clone();
            wasm_bindgen_futures::spawn_local(async move {
                let fetched_users: Vec<User> = Request::get("/service/users")
                    .send()
                    .await
                    .unwrap()
                    .json()
                    .await
                    .unwrap();
                users.set(fetched_users);
            });
            || ()
        });
    }
    users
}