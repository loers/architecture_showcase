use actix_web::{web, App, HttpServer};
use diesel::{
    r2d2::{self, ConnectionManager},
    PgConnection,
};

mod endpoints;
mod orm;
mod repositories;
mod schema;

pub type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let port = std::env::var("PORT")
        .expect("PORT")
        .parse::<u16>()
        .expect("PORT not a number");

    // set up database connection pool
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");
    let manager = ConnectionManager::<PgConnection>::new(db_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    log::info!("starting HTTP server at http://localhost:{}", port);

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(pool.clone()))
            .service(endpoints::list_users)
            .service(endpoints::create_report)
            .service(endpoints::list_reports)
            .service(endpoints::health)
    })
    .bind(("127.0.0.1", port))?
    .run()
    .await
}
