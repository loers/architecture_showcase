use actix_web::{Error, post};
use actix_web::{get, web, HttpResponse};
use domain::Report;

use crate::{repositories, DbPool};

#[get("/health")]
pub async fn health() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().body(""))
}

#[get("/users")]
pub async fn list_users(pool: web::Data<DbPool>) -> Result<HttpResponse, Error> {
    let users = web::block(move || {
        let mut conn = pool.get()?;
        repositories::find_users(&mut conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(users))
}

#[get("/reports")]
pub async fn list_reports(pool: web::Data<DbPool>) -> Result<HttpResponse, Error> {
    let reports = web::block(move || {
        let mut conn = pool.get()?;
        repositories::find_reports(&mut conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(reports))
}

#[post("/reports")]
pub async fn create_report(pool: web::Data<DbPool>, report: web::Json<Report>,) -> Result<HttpResponse, Error> {
    web::block(move || {
        let mut conn = pool.get()?;
        repositories::save_report(&mut conn, report.clone())
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().body(()))
}
