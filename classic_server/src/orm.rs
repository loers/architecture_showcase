use diesel::{
    prelude::{Insertable, Queryable},
    Selectable,
};
use domain::{DomainError, ReportNumber, UserName};

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::users)]
pub struct User {
    pub id: i32,
    pub user_name: String,
}

impl From<domain::User> for User {
    fn from(value: domain::User) -> Self {
        Self {
            id: 0,
            user_name: value.name_owned().value_owned(),
        }
    }
}

impl TryFrom<User> for domain::User {
    type Error = DomainError;

    fn try_from(value: User) -> Result<Self, Self::Error> {
        let user_name = UserName::try_from(value.user_name)?;
        let id = value.id.to_string();
        Ok(domain::User::from((id.into(), user_name)))
    }
}

#[derive(Queryable, Selectable, Insertable)]
#[diesel(table_name = crate::schema::reports)]
pub struct Report {
    pub id: i32,
    pub report_num: i32,
    pub report_creator: String,
}

impl From<domain::Report> for Report {
    fn from(value: domain::Report) -> Self {
        Self {
            id: 0,
            report_num: value.num().value(),
            report_creator: value.creator_owned().value_owned(),
        }
    }
}
impl From<(domain::Report, i32)> for Report {
    fn from((value, i): (domain::Report, i32)) -> Self {
        Self {
            id: i,
            report_num: value.num().value(),
            report_creator: value.creator_owned().value_owned(),
        }
    }
}

impl TryFrom<Report> for domain::Report {
    type Error = DomainError;

    fn try_from(value: Report) -> Result<Self, Self::Error> {
        let number = ReportNumber::try_from(value.report_num)?;
        let creator = UserName::try_from(value.report_creator)?;
        Ok(Self::new(creator, number))
    }
}

#[derive(Debug, Insertable)]
#[diesel(table_name = crate::schema::reports)]
pub struct NewReport {
    pub report_num: i32,
    pub report_creator: String,
}
impl From<domain::Report> for NewReport {
    fn from(value: domain::Report) -> Self {
        Self {
            report_creator: value.creator().to_string(),
            report_num: value.num().value(),
        }
    }
}
