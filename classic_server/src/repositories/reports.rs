use diesel::prelude::*;

use crate::orm::*;

type DbError = Box<dyn std::error::Error + Send + Sync>;

pub fn find_reports(conn: &mut PgConnection) -> Result<Vec<domain::Report>, DbError> {
    use crate::schema::reports::dsl::*;
    let result = reports.load::<Report>(conn)?;
    let result = result
        .into_iter()
        .filter_map(|u| match domain::Report::try_from(u) {
            Ok(r) => Some(r),
            Err(e) => {
                log::error!("{}", e);
                None
            }
        })
        .collect();
    Ok(result)
}

pub fn save_report(conn: &mut PgConnection, report: domain::Report) -> Result<(), DbError> {
    use crate::schema::reports::dsl::*;
    let r: NewReport = report.into();
    diesel::insert_into(reports)
        .values(r)
        .execute(conn)
        .unwrap();
    Ok(())
}
