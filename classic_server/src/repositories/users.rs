use diesel::prelude::*;

use crate::orm::*;

type DbError = Box<dyn std::error::Error + Send + Sync>;

pub fn find_users(conn: &mut PgConnection) -> Result<Vec<domain::User>, DbError> {
    use crate::schema::users::dsl::*;
    let result = users.load::<User>(conn)?;
    let result = result
        .into_iter()
        .filter_map(|u| domain::User::try_from(u).ok())
        .collect();
    Ok(result)
}
