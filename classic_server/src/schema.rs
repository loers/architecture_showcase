// @generated automatically by Diesel CLI.

diesel::table! {
    metrics (id) {
        id -> Int4,
        metric_name -> Varchar,
        metrics_report_id -> Int4,
    }
}

diesel::table! {
    mvalues (id) {
        id -> Int4,
        val -> Int4,
    }
}

diesel::table! {
    reports (id) {
        id -> Int4,
        report_num -> Int4,
        report_creator -> Varchar,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        user_name -> Varchar,
    }
}

diesel::joinable!(metrics -> reports (metrics_report_id));

diesel::allow_tables_to_appear_in_same_query!(
    metrics,
    mvalues,
    reports,
    users,
);
