use test_utils::init_db;

mod test_utils;

#[test]
fn setup_testdata() {
    if std::env::var("TESTDATA").is_ok() {
        env_logger::init();
        init_db("postgres://username:password@127.0.0.1:5432/service_db");
    }
}
