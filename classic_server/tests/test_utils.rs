use diesel::{connection::SimpleConnection, Connection, PgConnection};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use testcontainers::Container;
use testcontainers_modules::{postgres::Postgres, testcontainers::clients::Cli};

const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

fn create_connection(connection: &str) -> anyhow::Result<PgConnection> {
    Ok(PgConnection::establish(connection)?)
}

pub fn start_db<'a>(cli: &'a Cli) -> (Container<'a, Postgres>, PgConnection, String) {
    let node = cli.run(Postgres::default());
    let url = format!(
        "postgres://postgres:postgres@127.0.0.1:{}/postgres",
        node.get_host_port_ipv4(5432)
    );
    log::info!("Started database at {}", url);
    let con = init_db(&url);
    (node, con, url)
}

pub fn init_db<'a>(url: &str) -> PgConnection {
    let test_data = include_str!("../tests/testdata.sql");
    log::info!("Running migrations...");
    let mut con = create_connection(&url).expect("Failed to connect to test db");
    con.run_pending_migrations(MIGRATIONS)
        .expect("Failed to run migrations");
    con.batch_execute(test_data)
        .expect("Failed to run testdata.sql");
    con
}
