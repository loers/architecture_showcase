use std::net::TcpListener;

use domain::User;
use testcontainers_modules::testcontainers::clients::Cli;

use crate::test_utils::{init_db, start_db};

mod test_utils;

fn start_service(db_url: String) -> anyhow::Result<(std::process::Child, u16)> {
    let loglevel = std::env::var("RUST_LOG").unwrap_or_else(|_| "info".to_string());
    let service_port = TcpListener::bind("127.0.0.1:0")?.local_addr()?.port();

    let service = test_bin::get_test_bin("classic_server")
        .env("RUST_LOG", loglevel)
        .env("PORT", service_port.to_string())
        .env("DATABASE_URL", db_url)
        .stdout(std::process::Stdio::piped())
        .spawn()
        .unwrap();
    let retires = 10;
    for _ in 0..retires {
        log::info!("Waiting for service to be reachable...");
        std::thread::sleep(std::time::Duration::from_millis(200));
        if ureq::get(&format!("http://127.0.0.1:{}/health", service_port))
            .call()
            .is_ok()
        {
            return Ok((service, service_port));
        }
    }
    return Err(std::io::Error::new(
        std::io::ErrorKind::NotFound,
        "Unable to connect to test service",
    )
    .into());
}

#[test]
fn test_users() -> anyhow::Result<()> {
    let docker = Cli::default();
    env_logger::init();
    let (node, _, db_url) = start_db(&docker);
    let (mut service, port) = start_service(db_url)?;

    let users: Vec<User> = ureq::get(&format!("http://127.0.0.1:{port}/users"))
        .call()?
        .into_json()?;

    assert_eq!(users.len(), 3);

    service.kill().unwrap();

    drop(node);
    Ok(())
}
