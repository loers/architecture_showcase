
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    user_name VARCHAR NOT NULL
);

CREATE TABLE reports (
    id SERIAL PRIMARY KEY,
    report_num INT NOT NULL,
    report_creator VARCHAR NOT NULL
);

CREATE TABLE metrics (
    id SERIAL PRIMARY KEY,
    metric_name VARCHAR NOT NULL,
    metrics_report_id INT NOT NULL,
    CONSTRAINT fk_metric_report
    FOREIGN KEY(metrics_report_id) 
        REFERENCES reports(id)
);

CREATE TABLE mvalues (
    id SERIAL PRIMARY KEY,
    val INT NOT NULL
);

INSERT INTO users ( id, user_name ) VALUES (
    0, 'John Doe'
);
INSERT INTO users ( id, user_name ) VALUES (
    1, 'Max Mustermann'
);

INSERT INTO reports ( id, report_num, report_creator ) VALUES (
    0, 1001, 'Max Mustermann'
);
INSERT INTO reports ( id, report_num, report_creator ) VALUES (
    1, 1002, 'John Doe'
);

INSERT INTO metrics ( id, metric_name, metrics_report_id ) VALUES (
    0, 'heights', 0
);
INSERT INTO metrics ( id, metric_name, metrics_report_id ) VALUES (
    1, 'heights', 1
);
INSERT INTO metrics ( id, metric_name, metrics_report_id ) VALUES (
    2, 'widths', 0
);
INSERT INTO metrics ( id, metric_name, metrics_report_id ) VALUES (
    3, 'widths', 1
);

INSERT INTO mvalues ( id, val ) VALUES ( 0, 84 );
INSERT INTO mvalues ( id, val ) VALUES ( 1, 75 );
INSERT INTO mvalues ( id, val ) VALUES ( 2, 87 );
INSERT INTO mvalues ( id, val ) VALUES ( 3, 39 );
INSERT INTO mvalues ( id, val ) VALUES ( 4, 74 );
INSERT INTO mvalues ( id, val ) VALUES ( 5, 79 );
INSERT INTO mvalues ( id, val ) VALUES ( 6, 14 );
INSERT INTO mvalues ( id, val ) VALUES ( 7, 72 );
INSERT INTO mvalues ( id, val ) VALUES ( 8, 12 );
INSERT INTO mvalues ( id, val ) VALUES ( 9,  9 );
INSERT INTO mvalues ( id, val ) VALUES ( 10, 90 );
INSERT INTO mvalues ( id, val ) VALUES ( 11, 24 );
INSERT INTO mvalues ( id, val ) VALUES ( 12, 35 );
INSERT INTO mvalues ( id, val ) VALUES ( 13, 41 );
INSERT INTO mvalues ( id, val ) VALUES ( 14, 18 );
INSERT INTO mvalues ( id, val ) VALUES ( 15, 74 );
INSERT INTO mvalues ( id, val ) VALUES ( 16, 18 );
INSERT INTO mvalues ( id, val ) VALUES ( 17, 79 );
INSERT INTO mvalues ( id, val ) VALUES ( 18, 13 );
INSERT INTO mvalues ( id, val ) VALUES ( 19, 91 );
