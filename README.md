# Setup

## Basics

1. Install Rust

    https://rustup.rs/

    ```
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```

2. Install just
    ```
    cargo install just
    ```

## classic_server

1. Install diesel (OR mapper tooling) cli

    https://diesel.rs/guides/getting-started

    ```
    apt install libpq-dev libmysqlclient-dev libsqlite3-dev
    cargo install diesel_cli
    echo DATABASE_URL=postgres://username:password@localhost/service_db  > .env
    diesel setup
    diesel migration generate create_initial_db
    ```

## yew_client

1. Instrall trunk
    ```
    cargo install trunk
    ```

2. Instrall tailwind css
    ```
    npm i -g tailwindcss
    ```


# Starting everything

every module has a justfile that allows to start the module via
```
just run
```